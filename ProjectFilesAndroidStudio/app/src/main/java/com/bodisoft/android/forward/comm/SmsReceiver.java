package com.bodisoft.android.forward.comm;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.bodisoft.android.forward.Constants;
import com.bodisoft.android.forward.R;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.SmsMessage;

public class SmsReceiver extends BroadcastReceiver {
	
	private static final String TAG = SmsReceiver.class.getName();

	@Override
	public void onReceive(Context context, Intent intent) {
		final Bundle bundle = intent.getExtras();
		
		final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);															
		
		final String prefStartMsg = sharedPref.getString(context.getResources().getString(R.string.preference_activation_sms), "");		
		final String prefStopMsg = sharedPref.getString(context.getResources().getString(R.string.preference_stop_sms), "");
		
	
		if (bundle != null) {
			StringBuilder msg = new StringBuilder();
			final Object[] pdusObj = (Object[]) bundle.get("pdus");
			String senderNum = "N/A";
			String messagePart = null;
			String contactName = "N/A";

			for (int i = 0; i < pdusObj.length; i++) {
				SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
				String phoneNumber = currentMessage.getDisplayOriginatingAddress();

				final ContentResolver contentResolver = context.getContentResolver();
				
				//@todo: refactor a bit
				 senderNum = phoneNumber;
				 messagePart = currentMessage.getDisplayMessageBody();
				 contactName = ContactsHelper.getContactDisplayNameByNumber(senderNum, contentResolver);
				
				msg.append(messagePart);
			} // end for loop
			
			final String strMsg = msg.toString();			
			//If not start or stop message, send new message broadcast
			if (!strMsg.equalsIgnoreCase(prefStartMsg) && !strMsg.equalsIgnoreCase(prefStopMsg)) {
				final String timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());	
				final String formattedMessage = "Text message, from: " + contactName + " " + senderNum +  " at " + timestamp+ ", message: " + strMsg;
				
				final Intent smsBroadcastIntent = new Intent(); 
				smsBroadcastIntent.setAction(Constants.BROADCAST_NEW_SMS);
				smsBroadcastIntent.putExtra(Constants.BROADCAST_DATA_KEY, formattedMessage);
				context.sendBroadcast(smsBroadcastIntent);	
			}			
		} // bundle is null
	}


}
