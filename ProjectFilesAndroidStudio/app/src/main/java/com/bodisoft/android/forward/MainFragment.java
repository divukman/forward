package com.bodisoft.android.forward;

import com.bodisoft.android.forward.services.ForwardService;
import com.bodisoft.android.forward.services.ServiceUtil;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

public class MainFragment  extends Fragment implements View.OnClickListener {

	ImageView mImgView = null; 	
	BroadcastReceiver mBroadcastReceiver = null;
	private AdView mAdView;

	public MainFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_main, container, false);
		
		final Button btnStart = (Button) rootView.findViewById(R.id.btnStartStop);
		final Button btnRemoveAdds = (Button) rootView.findViewById(R.id.btnRemoveAds);
		mImgView= (ImageView) rootView.findViewById(R.id.imgState);

		mAdView = (AdView) rootView.findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder()
				.addTestDevice("A9ECC4204660CEAB0F2BC122826AEB46")
				.build();
		mAdView.loadAd(adRequest);

		btnStart.setOnClickListener(this);
		btnRemoveAdds.setOnClickListener(this);		
		
		listenToStartStopEvents();
		
		return rootView;
	}
	
	private void listenToStartStopEvents() {		
		mBroadcastReceiver = new BroadcastReceiver() {

			@Override
			public void onReceive(Context context, Intent intent) {

				final String action = intent.getAction();

				if (action.equalsIgnoreCase(Constants.BROADCAST_NEW_CONTROL_START_SMS)) {
					setImage(true);
				} else if (action.equalsIgnoreCase(Constants.BROADCAST_NEW_CONTROL_STOP_SMS)) {
					setImage(false);
				}
			}

		};

		final Context ctx = getActivity().getApplicationContext();

		IntentFilter filter = new IntentFilter();
		filter.addAction(Constants.BROADCAST_NEW_CONTROL_START_SMS);
		filter.addAction(Constants.BROADCAST_NEW_CONTROL_STOP_SMS);
		ctx.registerReceiver(mBroadcastReceiver, filter);
	}
	
	@Override
	public void onResume() {		
		super.onResume();	

		setImage(ServiceUtil.isMyServiceRunning(ForwardService.class, getActivity()));		
	}
	
	
	private void setImage(final boolean val) {
		if (val) {
			mImgView.setBackgroundResource(R.drawable.on);
		} else {
			mImgView.setBackgroundResource(R.drawable.off);
		}
	}

	@Override
	public void onClick(View v) {
		if (v!= null) { //cannot happen
			switch (v.getId()) {
			case R.id.btnStartStop:			
				
				boolean started = StartupHelper.startApplication(getActivity());
				if (started) {
					mImgView.setBackgroundResource(R.drawable.on);
				}
								
				break;
			case R.id.btnRemoveAds:
				boolean stopped = StartupHelper.stopApplication(getActivity());
				if (stopped) {
					mImgView.setBackgroundResource(R.drawable.off);
				}
				break;
			}
		}
	}
	
	

		
}
