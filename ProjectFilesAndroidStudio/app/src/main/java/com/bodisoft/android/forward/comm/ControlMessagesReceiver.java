package com.bodisoft.android.forward.comm;

import com.bodisoft.android.forward.Constants;
import com.bodisoft.android.forward.R;
import com.bodisoft.android.forward.StartupHelper;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.SmsMessage;
import android.util.Log;

/**
 * This receiver will listen to text messages and when control message
 * comes in, it will start or stop the service.
 * */
public class ControlMessagesReceiver extends BroadcastReceiver {
	
	private static final String TAG = ControlMessagesReceiver.class.getName();

	@Override
	public void onReceive(Context context, Intent intent) {
		final Bundle bundle = intent.getExtras();
		
		Log.d(TAG, "onReceive");
		
		if (bundle != null) {
			StringBuilder msg = new StringBuilder();
			final Object[] pdusObj = (Object[]) bundle.get("pdus");

			for (int i = 0; i < pdusObj.length; i++) {
				final SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);	
				final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);							
				final String message = currentMessage.getDisplayMessageBody();						
				
				final String prefStartMsg = sharedPref.getString(context.getResources().getString(R.string.preference_activation_sms), "");		
				final String prefStopMsg = sharedPref.getString(context.getResources().getString(R.string.preference_stop_sms), "");
				
				if (message.equals(prefStartMsg)) {
					final Intent smsBroadcastIntent = new Intent(); 
					smsBroadcastIntent.setAction(Constants.BROADCAST_NEW_CONTROL_START_SMS);					
					context.sendBroadcast(smsBroadcastIntent);
				} else if (message.equals(prefStopMsg)) {
					final Intent smsBroadcastIntent = new Intent(); 
					smsBroadcastIntent.setAction(Constants.BROADCAST_NEW_CONTROL_STOP_SMS);					
					context.sendBroadcast(smsBroadcastIntent);
				}
				
			} // end for loop
			
		} // bundle is null
	}

}
