package com.bodisoft.android.forward.comm;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.bodisoft.android.forward.Constants;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
//taken from http://www.learn-android-easily.com/2013/06/detect-missed-call-in-android.html
import android.util.Log;

public class CallReceiver extends BroadcastReceiver {

	private static final String TAG = CallReceiver.class.getSimpleName();
	
	static boolean ring = false;
	static boolean callReceived = false;
	static String callerPhoneNumber = null;
	private Context mContext = null;

	@Override
	public void onReceive(Context context, Intent intent) {		
		Log.d(TAG, "Forward: Incoming call.");
		mContext = context;
		// Get the current Phone State
		String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);

		if (state == null) {
			return; 
		}

		// If phone state "Rininging"
		if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {			
			// Get the Caller's Phone Number
			Bundle bundle = intent.getExtras();
			callerPhoneNumber = bundle.getString("incoming_number");
			Log.d(TAG, "Forward: Ringing: " + callerPhoneNumber);
			ring = true; 			
		} else if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) { // If incoming call is received
			Log.d(TAG, "Forward: Call received.");			
			callReceived = true;
		} if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) { // If phone is Idle			
			Log.d(TAG, "Forward: Phone idle.");
			// If phone was ringing(ring=true) and not
			// received(callReceived=false) , then it is a missed call
			Log.d(TAG, "Forward: Phone was ringing: " + ring + ", call received: " + callReceived);
			if (ring && !callReceived) {
				Log.d(TAG, "Forward: Detected missed call.");
				final String contactName = callerPhoneNumber != null ? ContactsHelper.getContactDisplayNameByNumber(callerPhoneNumber, context.getContentResolver()) : "Unknown";
				
				final String timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());	
				final String msg = "Missed call from : " + contactName + 
						" " + (callerPhoneNumber != null ? callerPhoneNumber : ". Number not available! (VOIP? [Skype?])") + " at " + timestamp + ".";								
				
				sendMissedCallBroadcast(msg);
			}
			
			callReceived = false;
			ring = false;
		}
	}
	
	private void sendMissedCallBroadcast(final String msg) {
		Log.d(TAG, "Forward: Sending missed call broadcast (" + msg + ").");
		final Intent smsBroadcastIntent = new Intent(); 
		smsBroadcastIntent.setAction(Constants.BROADCAST_MISSED_CALL);
		smsBroadcastIntent.putExtra(Constants.BROADCAST_DATA_KEY, msg.toString());
		mContext.sendBroadcast(smsBroadcastIntent);
	}
	
}
