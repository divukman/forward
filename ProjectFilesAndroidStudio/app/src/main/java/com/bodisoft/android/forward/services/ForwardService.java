package com.bodisoft.android.forward.services;

import com.bodisoft.android.forward.Constants;
import com.bodisoft.android.forward.MainActivity;
import com.bodisoft.android.forward.R;
import com.bodisoft.android.forward.comm.CallReceiver;
import com.bodisoft.android.forward.comm.SmsReceiver;
import com.bodisoft.android.forward.comm.mail.Mail;
import com.bodisoft.android.forward.network.NetworkManager;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.util.Log;

public class ForwardService extends Service {
	
	private static final String TAG = ForwardService.class.getName();
	private final int RETRIES = 10;
	
	private SmsReceiver mSmsReceiver = null;
	private CallReceiver mCallReceiver = null;
	private Context mContext = null;
	private IntentFilter mSMSReceivedIntentFilter = null;
	private IntentFilter mPhoneStateIntentFilter = null;
	
	private BroadcastReceiver mBroadcastReceiver = null;
	
	
	@Override
	public void onCreate() {	
		super.onCreate();
		
		mSmsReceiver = new SmsReceiver();
		mCallReceiver = new CallReceiver();
		mContext = getApplicationContext();
		
		mSMSReceivedIntentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
		mSMSReceivedIntentFilter.setPriority(android.content.IntentFilter.SYSTEM_HIGH_PRIORITY);
		
		mPhoneStateIntentFilter = new IntentFilter("android.intent.action.PHONE_STATE");
		mPhoneStateIntentFilter.setPriority(android.content.IntentFilter.SYSTEM_HIGH_PRIORITY);
				
		mBroadcastReceiver = new BroadcastReceiver() {
			
			@Override
			public void onReceive(Context context, Intent intent) {
				final String data = intent.getStringExtra(Constants.BROADCAST_DATA_KEY);
				Log.d(TAG, "Broadcast received: " + data);
				if (waitForNetworkConnection(context)) {
					new MailTask().execute(data);	
				}
			}
		};
		
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);

	}
	
	private boolean waitForNetworkConnection(final Context context) {		
		final NetworkManager netMgr = NetworkManager.getInstance();		
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);				
		final boolean useWifi = sharedPref.getBoolean(context.getResources().getString(R.string.preference_use_wifi), false);
		
		boolean connected = netMgr.isNetworkAvailable(context);		
		Log.d(TAG, "Checking for network connectivity. Connected: " + connected);
		
		if (!connected) {
			Log.d(TAG, "Using WiFi: " + useWifi);
			if (useWifi) {
				Log.d(TAG, "Not connected. Trying to start WiFi.");
				connected = netMgr.enableAndWaitForNetwork(context);								
				Log.d(TAG, "Establish WiFi connection: " + connected);
			}
		} 
		
		//Tries to connect to google.com with up to 30 retries!
		boolean googleReachable = false;
		if (connected) {
			googleReachable = netMgr.IsReachable(context);
		}
		
		Log.d(TAG, "Connection established: " + connected);
		return connected && googleReachable;
	}
	
	
	@Override
	public IBinder onBind(Intent intent) {		
		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {	
		
		 if (intent.getAction().equals(Constants.ACTION.STARTFOREGROUND_ACTION)) {
			 Log.d(TAG, "Received Start Foreground Intent "); 
			 	startForegroundAndShowNotification();
				registerReceivers();											 
		 } else if (intent.getAction().equals(Constants.ACTION.STOPFOREGROUND_ACTION)) {
			 Log.d(TAG, "Received Stop Foreground Intent");
			 unregisterReceivers();
			 stopForeground(true);
			 stopSelf();			
		 }
		 return START_STICKY;
	}
	
	@Override
	public void onDestroy() {		
		super.onDestroy();
	}
	
	
	private void registerReceivers() {			
		//Create two receivers. One will listen to SMS messages and other to phone state (call)
		mContext.registerReceiver(mSmsReceiver, mSMSReceivedIntentFilter);	//sms receiver listens for OS event of incoming text message
		//this receiver will send a broadcast when new text message arrives			
		mContext.registerReceiver(mCallReceiver, mPhoneStateIntentFilter); //this registers a listener to phone state, this listener will broadcast once we have missed call
		
		
		//listen to broadcasts of: sms broadcast, missed call broadcast
		IntentFilter filter = new IntentFilter();
		filter.addAction(Constants.BROADCAST_NEW_SMS);
		filter.addAction(Constants.BROADCAST_MISSED_CALL);
		mContext.registerReceiver(mBroadcastReceiver, filter); 
	}
	
	private void unregisterReceivers() {
		mContext.unregisterReceiver(mBroadcastReceiver);
		mContext.unregisterReceiver(mSmsReceiver);
		mContext.unregisterReceiver(mCallReceiver);		
	}
	
	private void startForegroundAndShowNotification() {   
	    // The PendingIntent to launch our activity if the user selects this notification
	    PendingIntent operation = PendingIntent.getActivity(this, 0,
	            new Intent(this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

		
		Notification notification = new Notification.Builder(mContext)
				.setContentTitle("Forward")
				.setContentText("Service Running.")
				.setSmallIcon(android.R.drawable.sym_action_email)
				.setContentIntent(operation)
				.setAutoCancel(false)				
				.build();
		
		startForeground(Constants.NOTIFICATION_ID.FOREGROUND_FORWARD_SERVICE, notification);
	}

	
	public class MailTask extends AsyncTask<String, Void, Boolean> {				

		@Override
		protected Boolean doInBackground(String... params) {
			
			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(mContext);
			final String prefForwardToEmail = sharedPref.getString(mContext.getResources().getString(R.string.preference_forward_to_email), "");		
			final String prefSMTPServer = sharedPref.getString(mContext.getResources().getString(R.string.preference_smtp_server), "");
			
			final String prefSMTPUsername = sharedPref.getString(mContext.getResources().getString(R.string.preference_smtp_username), "");		
			final String prefSMTPPassword = sharedPref.getString(mContext.getResources().getString(R.string.preference_smtp_password), "");
			
			
								
			Mail m = new Mail(prefSMTPServer, prefSMTPUsername, prefSMTPPassword);

			String[] toArr = { prefForwardToEmail };
			m.setTo(toArr);
			m.setFrom(prefSMTPUsername);
			m.setSubject("Android Forward Message");
			m.setBody(params[0]); //@todo think about refactoring whole Mail class!

			int counter = 0;
			boolean sent = false;
			boolean usingIpv4 = false;
			
			while ((counter < RETRIES) && !sent) { 
				
				Log.d("Forward", "Trying to send e mail, try: " + counter); //0 up
				
				try { 
					if (m.send()) {
						sent=  true;
					} else {
						sent = false;
					}
				} catch (Exception e) {		
					//@todo refactor ipv4 fallback, move to network manager
					Log.e("Forward", "Mail.send() exception. Usping ipv4: " + usingIpv4);
					if (!usingIpv4) {
						Log.e("Forward", "Could not send email! Exception: ", e);
						Log.e("Forward", "Switching to IPV4");
						java.lang.System.setProperty("java.net.preferIPv4Stack", "true");
						java.lang.System.setProperty("java.net.preferIPv6Addresses", "false");
						usingIpv4 = true;						
					}					

					sent = false;
				}
				
				counter++;
			}
			
			if (usingIpv4) {
				Log.e("Forward", "Switching back to IPV6");
				java.lang.System.setProperty("java.net.preferIPv4Stack", "false");
				java.lang.System.setProperty("java.net.preferIPv6Addresses", "true");
			}

			return sent;
		}
		
		@Override
		protected void onPostExecute(Boolean result) {			
			super.onPostExecute(result);			
		}

	}
}
