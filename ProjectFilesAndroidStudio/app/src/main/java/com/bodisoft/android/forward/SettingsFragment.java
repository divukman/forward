package com.bodisoft.android.forward;

import com.bodisoft.android.forward.boot.BootReceiver;
import com.bodisoft.android.forward.comm.ControlMessagesReceiver;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.widget.Toast;



public class SettingsFragment extends PreferenceFragment {
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);

	        // Load the preferences from an XML resource
	        addPreferencesFromResource(R.xml.preferences);
	        
	        //Listen to preference changed events
	        registerPreferenceListeners();
	    }

	 
	 /***
	  * Listens to specific preference(s) we are interested in.
	  */
	 protected void registerPreferenceListeners() {
		 final String prefKeyString = getString(R.string.preference_activate_via_sms);
		 final CheckBoxPreference prefStartViaText = (CheckBoxPreference)findPreference(prefKeyString); 
		 
		 
		 prefStartViaText.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				final boolean val = ((Boolean)newValue).booleanValue();
				Toast.makeText(getActivity(), "Activate via SMS: " + newValue, Toast.LENGTH_SHORT).show();
				prefStartViaText.setChecked(val);
								
				//Start or stop the control service now, so it works right away.
				if (val) {
					StartupHelper.startControlService(getActivity().getApplicationContext());
				} else {
					StartupHelper.stopControlService(getActivity().getApplicationContext());
				}
				
				return true;
			}
		});
	 }
	 
/*	 private void enableBootReceiver(final boolean enabled) {
		 final Context context = this.getActivity().getApplicationContext();
		 final int flag = (enabled ? PackageManager.COMPONENT_ENABLED_STATE_ENABLED :PackageManager.COMPONENT_ENABLED_STATE_DISABLED);
		 ComponentName component=new ComponentName(context, BootReceiver.class);

		 context.getPackageManager()
		     .setComponentEnabledSetting(component, flag,
		                                 PackageManager.DONT_KILL_APP);
	 }*/
}
