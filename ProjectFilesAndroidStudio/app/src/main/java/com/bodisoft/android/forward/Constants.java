package com.bodisoft.android.forward;

public final class Constants {
	
	private Constants() {
		throw new AssertionError (Constants.class.getName() + " can not be instantiated!");
	}

	public static final String BROADCAST_NEW_SMS = "com.bodisoft.android.forward.action.NEW_SMS";
	public static final String BROADCAST_NEW_MMS = "com.bodisoft.android.forward.action.NEW_MMS";
	public static final String BROADCAST_MISSED_CALL = "com.bodisoft.android.forward.action.MISSED_CALL";
	public static final String BROADCAST_DATA_KEY = "data";
	
	public static final String BROADCAST_NEW_CONTROL_START_SMS = "com.bodisoft.android.forward.action.NEW_CONTROL_START_SMS";
	public static final String BROADCAST_NEW_CONTROL_STOP_SMS = "com.bodisoft.android.forward.action.NEW_CONTROL_STOP_SMS";
	
	
	//Service constants
	public interface ACTION {
		 public static String STARTFOREGROUND_ACTION = "com.bodisoft.android.forward.action.startforeground";
		 public static String STOPFOREGROUND_ACTION = "com.bodisoft.android.forward.action.stopforeground";
	}
	
	public interface NOTIFICATION_ID {
		public static final int FOREGROUND_FORWARD_SERVICE = 1033;
		public static final int FOREGROUND_CONTROL_SERVICE = 2057;
	}
}
