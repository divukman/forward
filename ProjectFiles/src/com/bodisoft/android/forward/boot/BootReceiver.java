package com.bodisoft.android.forward.boot;

import com.bodisoft.android.forward.R;
import com.bodisoft.android.forward.StartupHelper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class BootReceiver extends BroadcastReceiver {
	
	private static final String TAG = BootReceiver.class.getSimpleName();

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(TAG, "onReceived");
		
		if (intent.getAction().equalsIgnoreCase(Intent.ACTION_BOOT_COMPLETED)) {
			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
			final boolean prefStartViaSMS = sharedPref
					.getBoolean(context.getResources().getString(R.string.preference_activate_via_sms), false);
			final boolean prefStartAfterReboot = sharedPref
					.getBoolean(context.getResources().getString(R.string.preference_start_after_reboot), false);

			if (prefStartViaSMS) {
				StartupHelper.startControlService(context);
			}

			if (prefStartAfterReboot) {
				StartupHelper.startApplication(context);
			}
		}
	}

}
