package com.bodisoft.android.forward;

import android.app.Activity;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.graphics.Color;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Gravity;

import android.view.Menu;
import android.view.MenuItem;

import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;


public class MainActivity extends Activity implements ActionBar.TabListener {
	
	private ViewPager mViewPager = null;
	private ActionBar mActionBar = null;
	private TabsPagerAdapter mAdapter = null;

	private static final String [] TABS = {"Forward", "Settings", "About"};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_ACTION_BAR);
		setContentView(R.layout.activity_main);
		
		getActionBar().setDisplayHomeAsUpEnabled(false);
		
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mActionBar = getActionBar();
		
		mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
		for (final String tab: TABS) {
			//Create a new view
			TextView t = new TextView(this);
			t.setLayoutParams(new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.MATCH_PARENT, 
					LinearLayout.LayoutParams.MATCH_PARENT, 
					Gravity.CENTER_VERTICAL));
			t.setGravity(Gravity.CENTER_VERTICAL);
			t.setTextColor(Color.WHITE);
			t.setText(tab);
			
			//Create a new tab
			final Tab actionBarTab = mActionBar.newTab();
			actionBarTab.setCustomView(t);
			actionBarTab.setTabListener(this);
			
			//Add tab to action bar
			mActionBar.addTab(actionBarTab);			
		}
		
		mAdapter = new TabsPagerAdapter(getFragmentManager());
		mViewPager.setAdapter(mAdapter);
		
		mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int position) {
				mActionBar.setSelectedNavigationItem(position);
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		startControlServiceIfNeeded();
	}

	
	private boolean startControlServiceIfNeeded() {
		return StartupHelper.startControlServiceOnStartup(MainActivity.this);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {		
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}
}
