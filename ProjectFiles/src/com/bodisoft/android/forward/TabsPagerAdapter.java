package com.bodisoft.android.forward;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;

public class TabsPagerAdapter extends FragmentPagerAdapter {

	public TabsPagerAdapter(FragmentManager fm) {
		super(fm);		
	}

	@Override
	public Fragment getItem(int index) {
		Fragment result = null;
		
		switch(index) {
			case 0:
				result = new MainFragment();
				break;
			case 1:
				result = new SettingsFragment();
				break;
			case 2:
				result = new AboutFragment();
				break;		
		}
		
		return result;
	}

	@Override
	public int getCount() {		
		return 3;
	}

}
