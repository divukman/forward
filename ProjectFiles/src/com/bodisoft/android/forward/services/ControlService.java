package com.bodisoft.android.forward.services;

import com.bodisoft.android.forward.Constants;
import com.bodisoft.android.forward.MainActivity;
import com.bodisoft.android.forward.StartupHelper;
import com.bodisoft.android.forward.comm.ControlMessagesReceiver;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;

public class ControlService extends Service {

	private static final String TAG = ControlService.class.getName();
	private ControlMessagesReceiver mSmsControlMessagesReceiver = null;
	private Context mContext = null;
	private IntentFilter mSMSReceivedIntentFilter = null;
	private BroadcastReceiver mBroadcastReceiver = null;
	//private NotificationManager mNM = null;
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		mSmsControlMessagesReceiver = new ControlMessagesReceiver();		
		mContext = getApplicationContext();
		mSMSReceivedIntentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
		mSMSReceivedIntentFilter.setPriority(android.content.IntentFilter.SYSTEM_HIGH_PRIORITY);			
		//mNM =  (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		
		mBroadcastReceiver = new BroadcastReceiver() {
			
			@Override
			public void onReceive(Context context, Intent intent) {
		
				final String action = intent.getAction();

				if (action.equalsIgnoreCase(Constants.BROADCAST_NEW_CONTROL_START_SMS)) {
					StartupHelper.startApplication(getApplicationContext());
				} else if (action.equalsIgnoreCase(Constants.BROADCAST_NEW_CONTROL_STOP_SMS)) {
					StartupHelper.stopApplication(getApplicationContext());
				}
			}
		};

	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {						
		//registerReceivers();		
		//showNotification("Control Service is running.");
				
		 if (intent.getAction().equals(Constants.ACTION.STARTFOREGROUND_ACTION)) {
			 Log.i(TAG, "Received Start Foreground Intent "); 
			 	startForegroundAndShowNotification();
				registerReceivers();											 
		 } else if (intent.getAction().equals(Constants.ACTION.STOPFOREGROUND_ACTION)) {
			 Log.i(TAG, "Received Stop Foreground Intent");
			 unregisterReceivers();
			 stopForeground(true);
			 stopSelf();			
		 }
		
		return START_STICKY;
	}
	
	@Override
	public void onDestroy() {		
		super.onDestroy();
		
		//unregisterReceivers();
		
		//mNM.cancel(NOTIFICATION);
	}
	
	
	private void registerReceivers() {			
		mContext.registerReceiver(mSmsControlMessagesReceiver, mSMSReceivedIntentFilter);	//sms receiver listens for OS event of incoming text message
		
		//listen to broadcasts of: sms broadcast, missed call broadcast
		IntentFilter filter = new IntentFilter();
		filter.addAction(Constants.BROADCAST_NEW_CONTROL_START_SMS);
		filter.addAction(Constants.BROADCAST_NEW_CONTROL_STOP_SMS);
		mContext.registerReceiver(mBroadcastReceiver, filter); 
	}
	
	private void unregisterReceivers() {
		mContext.unregisterReceiver(mBroadcastReceiver);
		mContext.unregisterReceiver(mSmsControlMessagesReceiver);
	}
	
	private void startForegroundAndShowNotification() {   
	    // The PendingIntent to launch our activity if the user selects this notification
	    PendingIntent operation = PendingIntent.getActivity(this, 0,
	            new Intent(this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

		
		Notification notification = new Notification.Builder(mContext)
				.setContentTitle("Forward")
				.setContentText("Control Service Running!")
				.setSmallIcon(android.R.drawable.stat_notify_sync)
				.setContentIntent(operation)
				.setAutoCancel(false)				
				.build();
		
		startForeground(Constants.NOTIFICATION_ID.FOREGROUND_CONTROL_SERVICE, notification);
	}

}
