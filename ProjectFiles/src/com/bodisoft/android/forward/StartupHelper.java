package com.bodisoft.android.forward;

import com.bodisoft.android.forward.services.ControlService;
import com.bodisoft.android.forward.services.ForwardService;
import com.bodisoft.android.forward.services.ServiceUtil;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

/***
 * Helper class used to extract code for starting and stopping the application, that is
 * for validating settings, registering and unregistering the broadcast receivers.
 * 
 * @author dvukman
 * @version 1.0
 */
public final class StartupHelper {
	
	private static final String TAG = StartupHelper.class.getName();

	//Suppress default constructor for noninstantiability
	private StartupHelper() {
		throw new AssertionError (StartupHelper.class.getName() + " can not be instantiated!");
	}
	
	
	public static boolean startApplication(final Context context) {
		boolean result = true;
		
		//Read and validate user settings
		final ValidationResult validSettings = StartupHelper.validateSettings(context);
		Log.d(TAG, "User settings valid: " + validSettings);
		final Intent intent = new Intent(context, ForwardService.class);
		intent.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
		
		if (ServiceUtil.isMyServiceRunning(ForwardService.class, context)) {
			Toast.makeText(context, "Already started!", Toast.LENGTH_SHORT).show();
		} else if (validSettings.result) { 					
			context.startService(intent);				
		} else {	
			result = false;
			Toast.makeText(context, validSettings.message, Toast.LENGTH_SHORT).show();
		} 
		
		
		return result;
	}

	
	public static boolean stopApplication(final Context context) {
		boolean result = true;
		
		//Disconnect broadcast receiver
		if (!ServiceUtil.isMyServiceRunning(ForwardService.class, context)) {
			Toast.makeText(context, "Already stopped!", Toast.LENGTH_SHORT).show();
		} else {
			final Intent intent = new Intent(context, ForwardService.class);
			intent.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
			//context.stopService(intent);
			context.startService(intent);
		}		
		
		return result;
	}
	
	
	private static ValidationResult validateSettings(final Context context) {
		final ValidationResult result = new ValidationResult();
		result.result = true;
		
		StringBuilder msg = new StringBuilder();
		
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
		final boolean prefStartViaSMS = sharedPref.getBoolean(context.getResources().getString(R.string.preference_activate_via_sms), false);
		//final boolean prefStartAfterReboot = sharedPref.getBoolean(context.getResources().getString(R.string.preference_start_after_reboot), false);
		
		final String prefActivationSMS = sharedPref.getString(context.getResources().getString(R.string.preference_activation_sms), context.getResources().getString(R.string.pref_key_start_text_msg_default));
		final String prefStopSMS = sharedPref.getString(context.getResources().getString(R.string.preference_stop_sms), context.getResources().getString(R.string.pref_key_stop_text_msg_default));
		
		//final boolean prefUseWiFi = sharedPref.getBoolean(context.getResources().getString(R.string.preference_use_wifi), false);
		//final boolean prefUseMobile = sharedPref.getBoolean(context.getResources().getString(R.string.preference_use_mobile), false);
		
		final String prefForwardToEmail = sharedPref.getString(context.getResources().getString(R.string.preference_forward_to_email), "");		
		final String prefSMTPServer = sharedPref.getString(context.getResources().getString(R.string.preference_smtp_server), "");
		
		final String prefSMTPUsername = sharedPref.getString(context.getResources().getString(R.string.preference_smtp_username), "");		
		final String prefSMTPPassword = sharedPref.getString(context.getResources().getString(R.string.preference_smtp_password), "");
		
		
		if (prefStartViaSMS && ( TextUtils.isEmpty(prefActivationSMS) || TextUtils.isEmpty(prefStopSMS ))) {
			msg.append("control messages ");
			result.result = false;
		}
		
		if (TextUtils.isEmpty(prefForwardToEmail) || TextUtils.isEmpty(prefSMTPServer ) || TextUtils.isEmpty(prefSMTPUsername ) || TextUtils.isEmpty(prefSMTPPassword)) {
			msg.append( (!result.result ? "and " : "" ) + "e mail settings");
			result.result = false;
		}
		
//		final String toastmsg = "Activate via SMS: " + prefStartViaSMS + ", start after reboot: " + prefStartAfterReboot + 
//				", activation sms: " + prefActivationSMS + ", stop sms: " + prefStopSMS + ", use wifi: " + prefUseWiFi +
//				", use mobile: " + prefUseMobile + ", email to: " + prefForwardToEmail + ", smtp server: " + prefSMTPServer + "( " + prefSMTPUsername + ", " + prefSMTPPassword + ")";
//		
//		int duration = Toast.LENGTH_LONG;
//		Toast.makeText(context, toastmsg, duration).show();
		
		result.message = "Please configure:  " + msg + ".";		
		return result;
	}
	
	private static class ValidationResult { 
		public boolean result;
		public String message;
	}
	
	public static boolean startControlService(final Context context) {
		boolean result = true;
		
		 Intent serviceIntent = new Intent(context, ControlService.class);	
		 serviceIntent.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
		 context.startService(serviceIntent);	
		 
		return result;
	}
	
	public static boolean stopControlService(final Context context) {
		boolean result = true;
		
		 Intent serviceIntent = new Intent(context, ControlService.class);	
		 serviceIntent.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
		 context.stopService(serviceIntent);	
		
		return result;
	}
	
	public static boolean startControlServiceOnStartup(final Context context) {
		boolean result = true;
		
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
		final boolean prefStartViaSMS = sharedPref.getBoolean(context.getResources().getString(R.string.preference_activate_via_sms), false);
		
		if (prefStartViaSMS) { 
			//Read and validate user settings
			final ValidationResult validSettings = StartupHelper.validateSettings(context);
			Log.d(TAG, "User settings valid: " + validSettings);
			final Intent intent = new Intent(context, ControlService.class);
			intent.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
			
			
			if (ServiceUtil.isMyServiceRunning(ControlService.class, context)) {
				Toast.makeText(context, "Forward control Service already running!", Toast.LENGTH_SHORT).show();
			} else if (validSettings.result) { 					
				context.startService(intent);			
			} else {	
				result = false;
				Toast.makeText(context, validSettings.message, Toast.LENGTH_SHORT).show();
			} 
		}
		
		return result;
	}
	
}
