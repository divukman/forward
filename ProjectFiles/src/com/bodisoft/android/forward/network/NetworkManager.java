package com.bodisoft.android.forward.network;

import java.net.HttpURLConnection;
import java.net.URL;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.StrictMode;
import android.util.Log;

public final class NetworkManager {
	
	private static final int TIMEOUT = 30; //timeout 
	private static final int RETRY_ATTEMPTS = 20;
	private static final String TAG = NetworkManager.class.getSimpleName();
	private final int IPV4_FALLBACK_RETRIES = 3;
	
	private NetworkManager() {		
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy); 
	}
	
	private static class SingletonHolder {
		public static final NetworkManager instance = new NetworkManager();
	}
	
	public static NetworkManager getInstance() {
		return SingletonHolder.instance;
	}
	
	public boolean isNetworkAvailable(final Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
	
	private void setWifiState(final Context context, final boolean enabled) {		
		WifiManager wManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
		wManager.setWifiEnabled(enabled); //true or false		
	}
	
	/***
	 * This is a blocking synchronous call!
	 * Wait for up to 30 seconds to get connectivity.
	 * @param context
	 * @return true if connected, false if not connected or timeout happened.
	 */
	public boolean enableAndWaitForNetwork(final Context context) {		
		int timeoutCounter = 0; 
		
		//enable WiFi
		setWifiState(context, true);
		
		//wait for connection
		try {			
			while ((timeoutCounter < NetworkManager.RETRY_ATTEMPTS) && !isNetworkAvailable(context)) {
				Thread.sleep(1000);
				timeoutCounter++;
			}
		} catch (InterruptedException e) {	
			Log.e(NetworkManager.TAG, "Exception when turning wifi ON!" + e.getMessage());
			e.printStackTrace();
		}
		
		return isNetworkAvailable(context);
	}
	
	
	/***
	 * Checks if google.com is reachable. Will retry 30 seconds.
	 * This is necessary because after missed call, android will deactivate wifi/3g temporarily.
	 * @param context
	 * @return true if reachable, false otherwise
	 */
	public boolean IsReachable(Context context) {
		boolean isReachable = false;
		int timeoutCounter = 0;
		boolean usingIpv4 = false;

		// Check if server is reachable
		while ((timeoutCounter < NetworkManager.RETRY_ATTEMPTS) && !isReachable) {
			try {
				URL url = new URL("http://www.google.com");
				HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
				urlc.setRequestProperty("User-Agent", "Android Application");
				urlc.setRequestProperty("Connection", "close");
				urlc.setConnectTimeout(10 * 1000);
				urlc.connect();
				isReachable = (urlc.getResponseCode() == 200);
				
				if (!isReachable) {
					Thread.sleep(1000);
				}
				
				timeoutCounter++;
			} catch (Exception e) {
				Log.e(TAG, "Error opening connection to google.com" + e.getMessage());
	
				if ( (timeoutCounter >= (NetworkManager.RETRY_ATTEMPTS /2)) && !usingIpv4) {
					Log.e("Forward", "Could not connect to google");
					Log.e("Forward", "Switching to IPV4");
					java.lang.System.setProperty("java.net.preferIPv4Stack", "true");
					java.lang.System.setProperty("java.net.preferIPv6Addresses", "false");
					usingIpv4 = true;					
				}
			}
		}
		
		if (usingIpv4) {
			Log.e("Forward", "Switching back to IPV6");
			java.lang.System.setProperty("java.net.preferIPv4Stack", "false");
			java.lang.System.setProperty("java.net.preferIPv6Addresses", "true");
		}

		return isReachable;
	}
}
